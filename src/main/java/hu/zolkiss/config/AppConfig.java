package hu.zolkiss.config;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.sap.cloud.account.TenantContext;

@EnableWebMvc
@Configuration
public class AppConfig implements WebMvcConfigurer {

	@Bean
  public TenantContext getTenantContext() throws NamingException {
    return (TenantContext) new InitialContext().lookup("java:comp/env/TenantContext");
  }
}
