package hu.zolkiss.components;

import org.springframework.stereotype.Component;

@Component
public class TestComponent {
	public String getComponentInformation() {
    return "It works form the TestComponent";
  }
}
