package hu.zolkiss.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sap.cloud.account.Account;
import com.sap.cloud.account.TenantContext;

import hu.zolkiss.components.TestComponent;

@RestController
public class TestServlet {

  private final TestComponent testComponent;
  private TenantContext tenantContext;

  @Autowired
  public TestServlet(TestComponent testComponent, TenantContext tenantContext) {
    this.testComponent = testComponent;
    this.tenantContext = tenantContext;
  }

  @GetMapping(value = "test-get")
  public String testGet() {
    StringBuilder sb = new StringBuilder("It works! ");
    Account account = this.tenantContext.getTenant().getAccount();
    sb.append(this.testComponent.getComponentInformation()).append(System.getProperty("line.separator"));
    sb.append("Account[id=").append(account.getId()).append(", name=").append(account.getName()).append("]");
    return sb.toString();
  }
}
